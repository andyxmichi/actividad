@extends('template.index')
@section('estilos')
    <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="../../js/Gestion/bancos.js"></script>
<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>


@endsection
@section('title', 'Bancos')
@section('content')

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listado de Bancos</h3>                
                <button class="btn btn-success float-right" id="btn-add" name="btn-add"  ><i class="fas fa-plus-square"> Nuevo</i></button> 
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="bancos_table" class="table table-bordered table-striped" style="width: 100%;">
                  <thead >
                  <tr>
                    <th style="width: 5%;">Id</th>
                    <th style="width: 13%;">Tipo de cuenta</th>
                    <th style="width: 25%;">Bancos</th>
                    <th style="width: 5%;">Cuenta</th>
                    <th style="width: 5%;">Encargado</th>
                    <th style="width: 5%;">Firma</th>
                    <th style="width: 5%;">Estado</th>
                    <th style="width: 10%;">Usuario</th>
                    <th style="width: 10%;">Fecha</th>
                    <th style="width: 20%;">Acciones</th>
                  </tr>
                  </thead>
                  <tbody id="bancos-list" name="bancos-list">
                  @foreach($bancos as $key => $banco)
                  <tr id="banco{{$banco->id}}">
                      <td>{{ $banco->id }}</td>
                      <td>{{ $banco->tipo_cuenta }}</td>
                      <td>{{ $banco->banco }}</td>
                      <td>{{ $banco->cuenta }}</td>
                      <td>{{ $banco->persona_encargada }}</td>
                      <td><img src="{{ $banco->firma }}" alt="" width="70" height="50"></td>
                      <td>{{ $banco->estado }}</td>
                      <td>{{ $banco->name }}</td>
                      <td>{{ $banco->updated_at }}</td>
                      <td>
                        <div class="btn-group">
                          <button class="btn btn-primary open-modal" value="{{$banco->id}}"><i class="fas fa-edit"></i></button>
                        </div>
                      </td>
                  @endforeach
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


    <div class="modal fade" id="bancoModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                              <label for="" name="bancoModalLabel" id="bancoModalLabel"></label>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <form id="modalFormData" name="modalFormData" class="form-horizontal"  enctype="multipart/form-data">
                            
                            <div class="form-group">
                                    <label class="col-sm-4 control-label">Tipo de cuenta:</label>
                                    <div class="col-sm-10">
                                        <select name="tipo_cuenta" id="tipo_cuenta"  tabindex ="6">
                                            <option value="AH">AHORRO</option>
                                            <option value="CTE">CORRIENTE</option>                                        
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Banco:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="banco" name="banco" placeholder="Ingrese nombre del Banco" tabindex ="2" value="">
                                        @error('empresa')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Número de cuenta:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="cuenta" name="cuenta" placeholder="Ingrese número de cuenta" tabindex ="3" value="">
                                        @error('razon_social')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Persona Encargada:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="persona_encargada" name="persona_encargada" placeholder="Ingrese persona encargada" tabindex ="4" value="">
                                        @error('ruc')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Firma:</label>
                                    <div class="col-sm-10">
                                      <input type="file" name="file1" class="form-control" id="image-input">
                                      <span class="text-danger" id="image-input-error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Estado:</label>
                                    <div class="col-sm-10">
                                      <select name="estado" id="estado"  tabindex ="6">
                                        <option value="A">Activo</option>
                                        <option value="I">Inactivo</option>                                        
                                      </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" name="btn-save" value="add" tabindex ="7" >Guardar
                            </button>
                            <input type="hidden" id="banco_id" name="banco_id" value="0">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
