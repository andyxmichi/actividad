@extends('template.index')
@section('estilos')
    <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="../../js/Gestion/departamentos.js"></script>
<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>


@endsection
@section('title', 'Departamentos')
@section('content')

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listado de departamentos</h3>                
                <button class="btn btn-success float-right" id="btn-add" name="btn-add"  ><i class="fas fa-plus-square"> Nuevo</i></button> 
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="departamentos_table" class="table table-bordered table-striped" style="width: 100%;">
                  <thead >
                  <tr>
                    <th style="width: 5%;">Id</th>                    
                    <th style="width: 25%;">Departamentos</th>
                    <th style="width: 5%;">Departamento Padre</th>
                    <th style="width: 5%;">Estado</th>
                    <th style="width: 10%;">Usuario</th>
                    <th style="width: 10%;">Fecha</th>
                    <th style="width: 20%;">Acciones</th>
                  </tr>
                  </thead>
                  <tbody id="departamentos-list" name="departamentos-list">
                  @foreach($departamentos as $key => $departamento)
                  <tr id="departamento{{$departamento->id}}">
                      <td>{{ $departamento->id }}</td>                      
                      <td>{{ $departamento->departamento }}</td>
                      <td>{{ $departamento->id_padre }}</td>
                      <td>{{ $departamento->estado }}</td>
                      <td>{{ $departamento->name }}</td>
                      <td>{{ $departamento->updated_at }}</td>
                      <td>
                        <div class="btn-group">
                          <button class="btn btn-primary open-modal" value="{{$departamento->id}}"><i class="fas fa-edit"></i></button>
                        </div>
                      </td>
                  @endforeach
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


    <div class="modal fade" id="departamentoModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                              <label for="" name="departamentoModalLabel" id="departamentoModalLabel"></label>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <form id="modalFormData" name="modalFormData" class="form-horizontal"  enctype="multipart/form-data">
                            
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Departamento:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="departamento" name="departamento" placeholder="Ingrese nombre del departamento" tabindex ="2" value=""  >
                                        @error('empresa')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Departamento Padre:</label>
                                    <div class="col-sm-10">
                                        <input type="radio" id="padreSi" name="padreSi" value="SI">
                                        <label for="padreSi">SI</label>
                                        <input type="radio" id="padreNo" name="padreNo" value="NO">
                                        <label for="padreNo">No</label>

                                        <input type="text" class="form-control" id="id_padre" name="id_padre" placeholder="Ingrese número de cuenta" tabindex ="3" value="" diable="true">
                                        @error('razon_social')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>                                                          
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Estado:</label>
                                    <div class="col-sm-10">
                                      <select name="estado" id="estado"  tabindex ="6">
                                        <option value="A">Activo</option>
                                        <option value="I">Inactivo</option>                                        
                                      </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" name="btn-save" value="add" tabindex ="7" >Guardar
                            </button>
                            <input type="hidden" id="departamento_id" name="departamento_id" value="0">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
