@extends('template.index')
@section('estilos')
    <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="../../js/Gestion/empresas.js"></script>
<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>


@endsection
@section('title', 'Empresas')
@section('content')

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listado de Empresas</h3>
                
                <button class="btn btn-success float-right" id="btn-add" name="btn-add"  ><i class="fas fa-plus-square"> Nuevo</i></button>
                <a href="/excel" class="btn btn-primary float-right" ><i class="far fa-file-excel"></i></a>
                
                  </button> 
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="empresas_table" class="table table-bordered table-striped" style="width: 100%;">
                  <thead >
                  <tr>
                    <th style="width: 5%;">Id</th>
                    <th style="width: 5%;">Codigo</th>
                    <th style="width: 25%;">Empresa</th>
                    <th style="width: 5%;">RUC</th>
                    <th style="width: 5%;">Icono</th>
                    <th style="width: 5%;">Estado</th>
                    <th style="width: 10%;">Usuario</th>
                    <th style="width: 10%;">Fecha</th>
                    <th style="width: 20%;">Acciones</th>
                  </tr>
                  </thead>
                  <tbody id="empresas-list" name="empresas-list">
                  @foreach($empresas as $key => $empresa)
                  <tr id="empresa{{$empresa->id}}">
                      <td>{{ $empresa->id }}</td>
                      <td>{{ $empresa->codigo_emp }}</td>
                      <td>{{ $empresa->empresa }}</td>
                      <td>{{ $empresa->ruc }}</td>
                      <td><img src="{{ $empresa->icono }}" alt="" width="70" height="50"></td>
                      <td>{{ $empresa->estado }}</td>
                      <td>{{ $empresa->name }}</td>
                      <td>{{ $empresa->updated_at }}</td>
                      <td>
                        <div class="btn-group">
                          <button class="btn btn-primary open-modal" value="{{$empresa->id}}"><i class="fas fa-edit"></i></button>
                        </div>
                      </td>
                  @endforeach
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


    <div class="modal fade" id="empresaModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                              <label for="" name="empresaModalLabel" id="empresaModalLabel"></label>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <form id="modalFormData" name="modalFormData" class="form-horizontal"  enctype="multipart/form-data">
                            
                            <div class="form-group">
                                    <label class="col-sm-4 control-label">Código empresa:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="codigo_emp" name="codigo_emp" placeholder="Ingrese código de la empresa" tabindex ="1" value="">
                                        @error('codigo_emp')
                                        <br >
                                          <small >{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nombre:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="empresa" name="empresa" placeholder="Ingrese nombre de la empresa" tabindex ="2" value="">
                                        @error('empresa')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Razon Social:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="razon_social" name="razon_social" placeholder="Ingrese razon social" tabindex ="3" value="">
                                        @error('razon_social')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ruc:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="ruc" name="ruc" placeholder="Ingrese Ruc o numero de cedula" tabindex ="4" value="">
                                        @error('ruc')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Direccion:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Ingrese direccion o numero de cedula" tabindex ="4" value="">
                                        @error('direccion')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Teléfono:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Ingrese telefono o numero de cedula" tabindex ="4" value="">
                                        @error('telefono')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Icono:</label>
                                    <div class="col-sm-10">
                                      <input type="file" name="file1" class="form-control" id="image-input">
                                      <span class="text-danger" id="image-input-error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Logo:</label>
                                    <div class="col-sm-10">                                        
                                      <input type="file" name="file2" class="form-control" id="image-input">
                                      <span class="text-danger" id="image-input-error-logo"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Estado:</label>
                                    <div class="col-sm-10">
                                      <select name="estado" id="estado"  tabindex ="6">
                                        <option value="A">Activo</option>
                                        <option value="I">Inactivo</option>                                        
                                      </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" name="btn-save" value="add" tabindex ="7" >Guardar
                            </button>
                            <input type="hidden" id="empresa_id" name="empresa_id" value="0">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
