@extends('template.index')
@section('estilos')
    <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="../../js/Gestion/empresas.js"></script>
<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>


@endsection
@section('title', 'Colaborador')
@section('content')

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listado de Colaborador</h3>
                
                <button class="btn btn-success float-right" id="btn-add" name="btn-add"  ><i class="fas fa-plus-square"> Nuevo</i></button>
                <a href="/excel" class="btn btn-primary float-right" ><i class="far fa-file-excel"></i></a>
                
                  </button> 
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="personales-tab" data-toggle="tab" href="#personales" role="tab" aria-controls="personales" aria-selected="true">Datos Personales</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="institucionales-tab" data-toggle="tab" href="#institucionales" role="tab" aria-controls="institucionales" aria-selected="false">Datos Institucionales</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="beneficios-tab" data-toggle="tab" href="#beneficios" role="tab" aria-controls="contact" aria-selected="false">Beneficios</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="descuento-tab" data-toggle="tab" href="#descuento" role="tab" aria-controls="contact" aria-selected="false">Descuentos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="biometrico-tab" data-toggle="tab" href="#biometrico" role="tab" aria-controls="contact" aria-selected="false">Biometrico</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="personales" role="tabpanel" aria-labelledby="personales-tab"><br>    
                    <div class="row">                                                  
                        <div class="col-2" align="center">                        
                            <img src="storage/empresas/AS_ICO_20211004.jfif" alt="" width="90" height="100">
                        </div>      
                        <div class="col-2">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">ID:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese valor" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                            <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Código:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese valor" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>    
                        <div class="col-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Colaborador:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese valor" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Tipo documento:</span>
                                    </div>
                                    <select class="custom-select" id="inputGroupSelect01">
                                    <option selected>Seleccionar...</option>
                                    <option value="F">Femenino</option>
                                    <option value="M">Maculino</option>
                                    </select>  
                                </div>
                                </div>

                                <div class="col-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Cédula:</span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Ingrese valor" aria-label="Username" aria-describedby="basic-addon1">  
                                </div>
                                </div>
                            </div>
                        </div>                                         
                    </div>            

                    <div class="row">
                        <div class="col-3"><br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Primer Apellido:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese valor" aria-label="Username" aria-describedby="basic-addon1">  
                            </div>
                        </div>
                        <div class="col-3"> <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Segundo Apellido:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese valor" aria-label="Username" aria-describedby="basic-addon1">  
                            </div>
                        </div>
                        <div class="col-3"><br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Primer Nombre:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese valor" aria-label="Username" aria-describedby="basic-addon1">  
                            </div>
                        </div>
                        <div class="col-3"><br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Segundo Nombre:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese valor" aria-label="Username" aria-describedby="basic-addon1">  
                            </div>
                        </div>                        
                    </div>
                    <div class="row">                        
                        <div class="col-4"><br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Fecha de Nacimiento:</span>
                                </div>
                                <input type="date" class="form-control">  
                            </div>                       
                        </div>
                        <div class="col-2"> <br>
                             <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Sexo:</span>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                <option selected>Seleccionar...</option>
                                <option value="F">Femenino</option>
                                <option value="M">Maculino</option>
                            </select> 
                            </div>                                            
                        </div>
                        <div class="col-3"><br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Estado civil:</span>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                <option selected>Seleccionar...</option>
                                <option value="F">Soltero</option>
                                <option value="M">Casado</option>
                                <option value="M">Divorciado</option>
                                <option value="M">Unión Libre</option>
                                <option value="M">Viudo</option>
                                </select>    
                            </div>                                                   
                        </div>  
                        <div class="col-3"><br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Tipo de sangre:</span>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                <option selected>Seleccionar...</option>
                                <option value="OP">O+</option>
                                <option value="ON">O-</option>
                                <option value="AP">A+</option>
                                <option value="AN">A-</option>                                
                                </select>   
                            </div>                                                    
                        </div>                                                                   
                    </div>
                    <div class="row">
                        <div class="col-3"><br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Celular:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese celular"  id="primer_apellido" name="primer_apellido">  
                            </div>                           
                        </div>
                        <div class="col-3"><br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Teléfono:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese celular"  id="primer_apellido" name="primer_apellido">  
                            </div>                            
                        </div> 
                        <div class="col-6"> <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Correo:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese celular"  id="primer_apellido" name="primer_apellido">  
                            </div>
                        </div>     

                    </div>
                    <div class="row">                        
                        <div class="col-3"> <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">País:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese celular"  id="primer_apellido" name="primer_apellido">  
                            </div>
                        </div>
                        <div class="col-3"><br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Provincia:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese celular"  id="primer_apellido" name="primer_apellido">  
                            </div>
                        </div>
                        <div class="col-3"><br>
                        <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Ciudad:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese celular"  id="primer_apellido" name="primer_apellido">  
                            </div>
                        </div>                            
                        <div class="col-3"><br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Parroquia:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese celular"  id="primer_apellido" name="primer_apellido">  
                            </div>
                        </div>                    
                    </div>
                    <div class="row">                     
                        <div class="col-3"><br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Zona:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese celular"  id="primer_apellido" name="primer_apellido">  
                            </div>                            
                        </div>  
                        <div class="col-3"><br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Sector:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese celular"  id="primer_apellido" name="primer_apellido">  
                            </div>
                        </div>  
                        <div class="col-6"><br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Domicilio:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese celular"  id="primer_apellido" name="primer_apellido">  
                            </div>                         
                        </div>
                                                                  
                    </div>
                    <div class="row">
                        <div class="col-5"><br>
                            <label for="">Contacto de Emergencia:</label>
                        </div>
                    </div>
                    <div class="row">                     
                        <div class="col-5"><br>                            
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Nombre y Apellido:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese celular"  id="primer_apellido" name="primer_apellido">  
                            </div>       
                        </div>
                        <div class="col-2"> <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Parentesco:</span>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                <option selected>Seleccionar...</option>
                                <option value="F">Femenino</option>
                                <option value="M">Maculino</option>
                                </select>
                            </div>                                             
                        </div>                       
                        <div class="col-3"><br>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Celular:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese celular separa con ;"  id="primer_apellido" name="primer_apellido">
                            </div>   
                        </div>                                         
                    </div>
                </div>
                <div class="tab-pane fade" id="institucionales" role="tabpanel" aria-labelledby="institucionales-tab"><br>
                    <div class="row">    
                        <div class="col-4">              
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Fecha de Ingreso:</span>
                                </div>
                                <input type="date" class="form-control" >
                            </div>
                        </div>   
                        <div class="col-4">              
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Fecha de Salida:</span>
                                </div>
                                <input type="date" class="form-control" >
                            </div>
                        </div>   
                        <div class="col-4">              
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Estado:</span>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected>Seleccionar...</option>
                                    <option value="A">Activo</option>
                                    <option value="R">Retirado</option>
                                </select>
                            </div>
                        </div>                           
                    </div>
                    <div class="row">
                    <div class="col-4"> <br>
                            <div class="input-group">   
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Area:</span>
                                </div>                         
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected>Seleccionar...</option>
                                    <option value="F">Administrativa</option>
                                    <option value="M">Varios</option>
                                </select> 
                            </div>                                            
                        </div>
                        <div class="col-4"><br>
                        <div class="input-group">   
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Departamento:</span>
                                </div>                         
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected>Seleccionar...</option>
                                    <option value="F">Administrativa</option>
                                    <option value="M">Varios</option>
                                </select> 
                            </div>                               
                        </div> 
                        <div class="col-4"><br>
                            <div class="input-group">   
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Cargo:</span>
                                </div>                         
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected>Seleccionar...</option>
                                    <option value="F">Administrativa</option>
                                    <option value="M">Varios</option>
                                </select> 
                            </div>                                 
                        </div>                                                                  
                    </div>
                    <div class="row">
                        <div class="col-4"> <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Tipo de empleado:</span>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected>Seleccionar...</option>
                                    <option value="F">Dependiente</option>
                                    <option value="M">Varios</option>
                                </select> 
                            </div>
                        </div>                         
                        <div class="col-2">  <br>            
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Discapacidad:</span>
                                </div>
                                <input type="checkbox" class="form-control" name="" id="">
                            </div>
                        </div>  
                        <div class="col-3">  <br>            
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">% de Discapacidad:</span>
                                </div>
                                <input type="number" class="form-control" name="" id="">
                            </div>
                        </div>  
                        <div class="col-3">  <br>            
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Tipo de Discapacidad:</span>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected>Seleccionar...</option>
                                    <option value="F">Dependiente</option>
                                    <option value="M">Varios</option>
                                </select>
                            </div>
                        </div>                          
                    </div>
                    <div class="row">
                    <div class="col-3">  <br>            
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">N° de contrato:</span>
                                </div>
                                <input type="text" class="form-control"id="contrato" name="contrato">
                            </div>
                        </div>
                        <div class="col-3"> <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Sueldo:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese valor" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-3"> <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Hora:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese valor" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>  
                        <div class="col-3">  <br>            
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Forma de Pago:</span>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected>Seleccionar...</option>
                                    <option value="F">Dependiente</option>
                                    <option value="M">Varios</option>
                                </select>
                            </div>
                        </div>                       
                    </div>
                    <div class="row">                         
                        <div class="col-3"> <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Banco:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese valor" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>  
                        <div class="col-3"> <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Número de cuenta:</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese valor" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>   
                        <div class="col-3">  <br>            
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Tipo de Cuenta:</span>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected>Seleccionar...</option>
                                    <option value="F">Dependiente</option>
                                    <option value="M">Varios</option>
                                </select>
                            </div>
                        </div> 
                        <div class="col-3">  <br>            
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Fondos de Reserva:</span>
                                </div>
                                <input type="checkbox" class="form-control" name="" id="">
                            </div>
                        </div>  
                    </div>
                    <div class="row">
                        <div class="col-6"> <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Correo institucional</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese valor" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-3">  <br>            
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Décimo Tercero:</span>
                                </div>
                                <input type="checkbox" class="form-control" name="" id="">
                            </div>
                        </div>  
                        <div class="col-3">  <br>            
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Décimo Cuarto:</span>
                                </div>
                                <input type="checkbox" class="form-control" name="" id="">
                            </div>
                        </div>  
                    </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="row">
                    <div class="col-2"> <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">asd</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Ingrese valor" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
                </div>                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


    <div class="modal fade" id="empresaModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                              <label for="" name="empresaModalLabel" id="empresaModalLabel"></label>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <form id="modalFormData" name="modalFormData" class="form-horizontal"  enctype="multipart/form-data">
                            
                            <div class="form-group">
                                    <label class="col-sm-4 control-label">Código empresa:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="codigo_emp" name="codigo_emp" placeholder="Ingrese código de la empresa" tabindex ="1" value="">
                                        @error('codigo_emp')
                                        <br >
                                          <small >{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nombre:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="empresa" name="empresa" placeholder="Ingrese nombre de la empresa" tabindex ="2" value="">
                                        @error('empresa')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Razon Social:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="razon_social" name="razon_social" placeholder="Ingrese razon social" tabindex ="3" value="">
                                        @error('razon_social')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ruc:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="ruc" name="ruc" placeholder="Ingrese Ruc o numero de cedula" tabindex ="4" value="">
                                        @error('ruc')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Direccion:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Ingrese direccion o numero de cedula" tabindex ="4" value="">
                                        @error('direccion')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Teléfono:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Ingrese telefono o numero de cedula" tabindex ="4" value="">
                                        @error('telefono')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Icono:</label>
                                    <div class="col-sm-10">
                                      <input type="file" name="file1" class="form-control" id="image-input">
                                      <span class="text-danger" id="image-input-error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Logo:</label>
                                    <div class="col-sm-10">                                        
                                      <input type="file" name="file2" class="form-control" id="image-input">
                                      <span class="text-danger" id="image-input-error-logo"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Estado:</label>
                                    <div class="col-sm-10">
                                      <select name="estado" id="estado"  tabindex ="6">
                                        <option value="A">Activo</option>
                                        <option value="I">Inactivo</option>                                        
                                      </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" name="btn-save" value="add" tabindex ="7" >Guardar
                            </button>
                            <input type="hidden" id="empresa_id" name="empresa_id" value="0">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
