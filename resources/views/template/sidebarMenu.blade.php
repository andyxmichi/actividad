<!-- Sidebar Menu -->
<nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-open">
            <a href="/dashboard3" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>            
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Gestion
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/empresas" class="nav-link">
                  <i class="fas fa-building nav-icon"></i>
                  <p>Empresas</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/bancos" class="nav-link">
                  <i class="fas fa-piggy-bank nav-icon"></i>
                  <p>Bancos</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/departamentos" class="nav-link">
                  <i class="fas fa-layer-group nav-icon"></i>
                  <p>Departamentos</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/areas" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Areas</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/cargos" class="nav-link">
                  <i class="far fa-address-card nav-icon"></i>
                  <p>Cargos</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/competencias" class="nav-link">
                  <i class="fas fa-hammer nav-icon"></i>
                  <p>Competencias</p>
                </a>
              </li>
            </ul>
          </li>   
          
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-friends"></i>
              <p>
                Colaborador
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/colaborador" class="nav-link">
                  <i class="fas fa-user-cog nav-icon"></i>
                  <p>Datos</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/bancos" class="nav-link">
                  <i class="fas fa-user-edit nav-icon"></i>
                  <p>Contratos</p>
                </a>
              </li>   
              <li class="nav-item">
                <a href="/bancos" class="nav-link">                  
                  <i class="fas fa-user-minus nav-icon"></i> 
                  <p>Descuentos</p>
                </a>
              </li>   
              <li class="nav-item">
                <a href="/bancos" class="nav-link">
                  <i class="fas fa-user-plus nav-icon"></i>
                  <p>Beneficios</p>
                </a>
              </li>              
            </ul>
          </li>  

          <li class="nav-header">EXAMPLES</li>
          <li class="nav-item">
            <a href="pages/calendar.html" class="nav-link">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Calendar
                <span class="badge badge-info right">2</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="pages/gallery.html" class="nav-link">
              <i class="nav-icon far fa-image"></i>
              <p>
                Gallery
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="pages/kanban.html" class="nav-link">
              <i class="nav-icon fas fa-columns"></i>
              <p>
                Kanban Board
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-envelope"></i>
              <p>
                Mailbox
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/mailbox/mailbox.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inbox</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/mailbox/compose.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Compose</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/mailbox/read-mail.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Read</p>
                </a>
              </li>
            </ul>
          </li>
                 
        </ul>
      </nav>
      <!-- /.sidebar-menu -->