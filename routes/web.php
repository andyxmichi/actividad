<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImageController;
use App\Exports\EmpresaExport;
use Maatwebsite\Excel\Facades\Excel;

use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/a', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');

Route::get('/',  function () {
    return view('template.index');
});

Route::get('/vuec',  function () {
    return view('pruebaVue');
});

//////////////////////////modulo de gestion////////////////////////////////////
Route::resource('empresas', 'App\Http\Controllers\Gestion\EmpresaController');
Route::get('empresas/validarruc/{id}', 'App\Http\Controllers\Gestion\EmpresaController@consultarRuc');
Route::get('/excel', function () {
    return Excel::download(new EmpresaExport, 'empresa.xlsx');
});

Route::resource('/bancos'  , 'App\Http\Controllers\Gestion\BancoController');
Route::get('bancos/validarcuenta/{id}', 'App\Http\Controllers\Gestion\BancoController@consultarCuenta');
Route::resource('/departamentos', 'App\Http\Controllers\Gestion\DepartamentoController');
Route::get('departamentos/validarDepartamento/{departamento}', 'App\Http\Controllers\Gestion\DepartamentoController@validarDepartamento');
Route::resource('/areas'   , 'App\Http\Controllers\Gestion\AreaController');
Route::resource('/cargos'  , 'App\Http\Controllers\Gestion\CargoController');
Route::resource('/competencia', 'App\Http\Controllers\Gestion\CompetenciaController');


Route::resource('/colaborador', 'App\Http\Controllers\Colaborador\ColaboradorController');



Route::get('upload-images', [ ImageController::class, 'index' ]);
Route::post('upload-images', [ ImageController::class, 'storeImage' ])->name('images.store');
