<?php

namespace App\Exports;

use App\Models\Empresa;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;

use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class EmpresaExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $empresas =  Empresa::select( 'empresas.codigo_emp' , 'empresas.ruc', 'empresas.empresa',
        'empresas.razon_social','empresas.direccion','empresas.telefono','empresas.icono','empresas.logo',
        'empresas.estado','users.name', 'empresas.created_at','empresas.updated_at')
        ->join('users', 'users.id', '=', 'empresas.usuario')
        ->get();

       /* $fila = 1;

        foreach($empresas as $key => $empresa){
            $fila = $fila +1;
            $drawing = new Drawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath(public_path($empresa->icono));
            $drawing->setCoordinates('N'.($fila));
            $drawing->setWorksheet($event->sheet->getDelegate());
        }
        */

        return $empresas;
    }

    public function headings(): array
    {
        return [
            
            'codigo_empresa',
            'ruc',
            'empresa',
            'razon_social',
            'direccion',
            'telefono',
            'icono',
            'logo',
            'estado',
            'usuario',
            'Created at',
            'Updated at'
        ];
    }

    

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                
                $products = Empresa::limit(10)->get();

                /*
                $loop = 1;
                foreach ($products as $product) {
                    $drawing = new Drawing();
                    $drawing->setName('image');
                    $drawing->setDescription('image');
                    $drawing->setPath(public_path($product->logo));                    
                    $drawing->setCoordinates('N'. $loop);
                    $drawing->setWorksheet($event->sheet->getDelegate());
                    $loop++;
                }*/
            },
        ];
    }
}
