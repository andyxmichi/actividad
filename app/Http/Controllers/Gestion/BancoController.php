<?php

namespace App\Http\Controllers\Gestion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banco;
use DB;

class BancoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bancos = Banco::select('bancos.id', 'bancos.banco','bancos.cuenta','bancos.tipo_cuenta','bancos.persona_encargada',
        'bancos.firma',
        'bancos.estado','users.name', 'bancos.updated_at')
        ->join('users', 'users.id', '=', 'bancos.usuario')
        ->get();

        return view('gestion.banco')
        ->with('bancos', $bancos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuario = auth()->user()->id;
        
        $banco = new banco;

        $ldate = date('Y').date('m').date('d');
        $request->validate([
            'file1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'file2' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
          ]);

        if ($request->file('file1')) {
            $imagePath = $request->file('file1');
            $imageName = $imagePath->getClientOriginalName();
            $imageName = $request->banco.'_ICO_'.$ldate.'.'.$imagePath->getClientOriginalextension();
            $pathIco = $request->file('file1')->storeAs('bancos', $imageName, 'public');
            $banco->firma = '/storage/'.$pathIco;
        }
               
        $banco->tipo_cuenta = $request->tipo_cuenta;
        $banco->banco = $request->banco;
        $banco->cuenta = $request->cuenta;
        $banco->persona_encargada = $request->persona_encargada;
        $banco->estado = $request->estado;
        $banco->usuario = $usuario;
        
        $banco->save();
        
        $bancos = Banco::select('bancos.id', 'bancos.banco','bancos.cuenta','bancos.tipo_cuenta','bancos.persona_encargada',
        'bancos.firma',
        'bancos.estado','users.name', 'bancos.updated_at')
        ->join('users', 'users.id', '=', 'bancos.usuario')
        ->where('bancos.id',$banco->id)
        ->get();
        
        return Response()->json($bancos);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banco = Banco::where('id', $id)->get();        
        return Response()->json($banco);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ldate = date('Y').date('m').date('d');
        $banco = banco::find($id);

        $request->validate([
            'file1' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            'file2' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
          ]);

        if ($request->file('file1')) {
            $imagePath = $request->file('file1');
            $imageName = $request->banco.'_ICO_'.$ldate.'.'.$imagePath->getClientOriginalextension();
            $pathIco = $request->file('file1')->storeAs('bancos', $imageName, 'public');
            $banco->firma     = '/storage/'.$pathIco;
        }     

        $banco->banco   = $request->banco;
        $banco->tipo_cuenta = $request->tipo_cuenta;
        $banco->cuenta = $request->cuenta;
        $banco->persona_encargada  = $request->persona_encargada;
        $banco->estado    = $request->estado;

        $banco->save();

        $bancos = Banco::select('bancos.id', 'bancos.banco','bancos.cuenta','bancos.tipo_cuenta','bancos.persona_encargada',
        'bancos.firma',
        'bancos.estado','users.name', 'bancos.updated_at')
        ->join('users', 'users.id', '=', 'bancos.usuario')
        ->where('bancos.id',$id)
        ->get();

        return Response()->json($bancos);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function consultarCuenta($cuenta)
    {
        $cuentas = Banco::where('cuenta', $cuenta)->get();        
        if (count($cuentas)>0){
            $validacion = 1;
        }else{
            $validacion = 0;
        }
        
        return $validacion;
    }
}
