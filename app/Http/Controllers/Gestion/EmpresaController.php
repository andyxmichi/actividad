<?php

namespace App\Http\Controllers\Gestion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Empresa;
use DB;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       // $empresas = Empresa::all();
        $empresas = Empresa::select('empresas.id', 'empresas.codigo_emp' , 'empresas.empresa','empresas.ruc',
                            'empresas.icono',
                            'empresas.estado','users.name', 'empresas.updated_at')
                            ->join('users', 'users.id', '=', 'empresas.usuario')
                            ->get();

        return view('gestion.empresa')
            ->with('empresas', $empresas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuario = auth()->user()->id;
        
        $empresa = new empresa;

        $ldate = date('Y').date('m').date('d');
        $request->validate([
            'file1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'file2' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
          ]);

        if ($request->file('file1')) {
            $imagePath = $request->file('file1');
            $imageName = $imagePath->getClientOriginalName();
            $imageName = $request->empresa.'_ICO_'.$ldate.'.'.$imagePath->getClientOriginalextension();
            $pathIco = $request->file('file1')->storeAs('empresas', $imageName, 'public');
            $empresa->icono = '/storage/'.$pathIco;
        }
        if ($request->file('file2')) {
          $imagePath = $request->file('file2');         
          $imageName = $request->empresa.'_LOGO_'.$ldate.'.'.$imagePath->getClientOriginalextension();
          $pathLogo = $request->file('file2')->storeAs('empresas', $imageName, 'public');
          $empresa->logo = '/storage/'.$pathLogo;
        }

       
        $empresa->codigo_emp = $request->codigo_emp;
        $empresa->empresa = $request->empresa;
        $empresa->razon_social = $request->razon_social;
        $empresa->ruc = $request->ruc;
        $empresa->direccion = $request->direccion;
        $empresa->telefono = $request->telefono;
        $empresa->estado = $request->estado;
        $empresa->usuario = $usuario;
        
        $empresa->save();
        
        $empresas = Empresa::select('empresas.id', 'empresas.codigo_emp' , 'empresas.empresa','empresas.ruc',
        'empresas.icono',
        'empresas.estado','users.name', 'empresas.updated_at')
        ->join('users', 'users.id', '=', 'empresas.usuario')
        ->where('empresas.id',$empresa->id)
        ->get();

        return Response()->json($empresas);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $empresa = Empresa::where('id', $id)->get();
        //var_dump($empresa);
        return Response()->json($empresa);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ldate = date('Y').date('m').date('d');
        $empresa = Empresa::find($id);

        $request->validate([
            'file1' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            'file2' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
          ]);

        if ($request->file('file1')) {
            $imagePath = $request->file('file1');
            $imageName = $request->empresa.'_ICO_'.$ldate.'.'.$imagePath->getClientOriginalextension();
            $pathIco = $request->file('file1')->storeAs('empresas', $imageName, 'public');
            $empresa->icono     = '/storage/'.$pathIco;
        }
        
        if ($request->file('file2')) {
          $imagePath = $request->file('file2');         
          $imageName = $request->empresa.'_LOGO_'.$ldate.'.'.$imagePath->getClientOriginalextension();
          $pathLogo = $request->file('file2')->storeAs('empresas', $imageName, 'public');
          $empresa->logo      = '/storage/'.$pathLogo;
        }        

        $empresa->empresa   = $request->empresa;
        $empresa->razon_social = $request->razon_social;
        $empresa->direccion = $request->direccion;
        $empresa->telefono  = $request->telefono;
        $empresa->estado    = $request->estado;

        $empresa->save();

        $empresas = Empresa::select('empresas.id', 'empresas.codigo_emp' , 'empresas.empresa','empresas.ruc',
        'empresas.icono',
        'empresas.estado','users.name', 'empresas.updated_at')
        ->join('users', 'users.id', '=', 'empresas.usuario')
        ->where('empresas.id',$empresa->id)
        ->get();

        return Response()->json($empresas);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function consultarRuc($ruc)
    {
        $rucs = Empresa::where('ruc', $ruc)->get();        
        if (count($rucs)>0){
            $validacion = 1;
        }else{
            $validacion = 0;
        }
        
        return $validacion;
    }
}
