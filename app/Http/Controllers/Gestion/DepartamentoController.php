<?php

namespace App\Http\Controllers\Gestion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Departamento;
use DB;

class DepartamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departamentos = Departamento::select('departamentos.id', 'departamentos.departamento','departamentos.id_padre',
        'departamentos.estado','users.name', 'departamentos.updated_at')
        ->join('users', 'users.id', '=', 'departamentos.usuario')
        ->get();

        return view('gestion.departamento')
        ->with('departamentos', $departamentos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuario = auth()->user()->id;
        
        $departamento = new departamento;             
        
        $departamento->departamento = $request->departamento;
        $departamento->id_padre = $request->id_padre;        
        $departamento->estado = $request->estado;
        $departamento->usuario = $usuario;
        
        $departamento->save();
        
        $departamentos = departamento::select('departamentos.id', 'departamentos.departamento','departamentos.id_padre',
        'departamentos.estado','users.name', 'departamentos.updated_at')
        ->join('users', 'users.id', '=', 'departamentos.usuario')
        ->where('departamentos.id',$departamento->id)
        ->get();
        
        return Response()->json($departamentos);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departamento = Departamento::where('id', $id)->get();        
        return Response()->json($departamento);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = auth()->user()->id;

        $departamento = Departamento::find($id);
        
        $departamento->departamento   = $request->departamento;
        $departamento->id_padre = $request->id_padre;
        $departamento->estado    = $request->estado;
        $departamento->usuario = $usuario;

        $departamento->save();

        $departamentos = departamento::select('departamentos.id', 'departamentos.departamento','departamentos.id_padre',
        'departamentos.estado','users.name', 'departamentos.updated_at')
        ->join('users', 'users.id', '=', 'departamentos.usuario')
        ->where('departamentos.id',$id)
        ->get();

        return Response()->json($departamentos);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validarDepartamento($departamento)
    {
        $departamentos = Departamento::where('departamento', $departamento)->get();        
        if (count($departamentos)>0){
            $validacion = 1;
        }else{
            $validacion = 0;
        }
        
        return $validacion;
    }
}
