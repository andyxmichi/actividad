<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Image;
use App\Models\Empresa;
use DB;

class ImageController extends Controller
{
    public function index()
    {

      return view('images');
    }

    public function storeImage(Request $request)
    {        
        $request->validate([
          'file1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
          'file2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        
        $image = new Image;

        if ($request->file('file1')) {
            $imagePath = $request->file('file1');
            $imageName = $imagePath->getClientOriginalName();

            $path = $request->file('file1')->storeAs('empresas', $imageName, 'public');
        }
        if ($request->file('file2')) {
          $imagePath = $request->file('file2');         
          $imageName = 'nombre'.'.'.$imagePath->getClientOriginalextension();
          
          $path = $request->file('file2')->storeAs('empresas', $imageName, 'public');
      }

        $image->name = $imageName;
        $image->path = '/storage/'.$path;
        $image->save();
        $p = DB::table('empresas')
              ->where('id',2 )
              ->update(['icono' =>  $image->id]);
              //$p->save();
        return response()->json('Imagen subida con exito');
    }
}