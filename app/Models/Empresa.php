<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;
    protected $fillable = [
        'codigo_emp', 'ruc', 'empresa', 'razon_social', 'direccion', 'telefono', 'icono', 'logo', 'estado', 'usuario'
    ];

}
