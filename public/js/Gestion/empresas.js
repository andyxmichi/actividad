
$(function () {
  $("#empresas_table").DataTable({
    "responsive": true, "lengthChange": false, "autoWidth": false,
    "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
  }).buttons().container().appendTo('#empresas_table_wrapper .col-md-6:eq(0)');
  $('#example2').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "responsive": true,
  });

  $('#btn-add').click(function () {
    jQuery('#btn-add').click(function () {
    jQuery('#codigo_emp').prop( "disabled", false );
    jQuery('#ruc').prop( "disabled", false );
    jQuery('#btn-save').val("add");
    jQuery('#modalFormData').trigger("reset");
    jQuery('#empresaModalLabel').text("Nueva Empresa");
    jQuery('#empresaModal').modal('show');  
    });
  });

  jQuery(function() {
    jQuery("#modalFormData").validate({
      rules: {
        codigo_emp: { required: true,minlength: 3,maxlength: 3},
        empresa: { required: true, minlength: 1, maxlength: 250},
        razon_social: { required: true, minlength: 1, maxlength: 500},
        ruc: { required: true,minlength: 13,maxlength: 13,number: true},
        estado: { required: true}
      },
      messages: {
        ruc: {
                required: "Campo obligatorio",
                minlength: $.format("Necesitamos por lo menos 13 caracteres"),
                maxlength: $.format("{0} caracteres son demasiados!"),
                number: "Solo puede ingresar números"
              },
        empresa: { required: "Campo obligatorio"},
        razon_social: { required: "Campo obligatorio"},
  
      }
     });
  });

  jQuery('body').on('click', '.open-modal', function () {
    var empresa_id = $(this).val();
    if ($(this).val().length > 0){
      jQuery('#codigo_emp').prop( "disabled", true );
      jQuery('#ruc').prop( "disabled", true );
    }
    $.get('empresas/' + empresa_id+'/edit', function (data) {       
      $.each(data, function(i){  
        jQuery('#empresa_id').val(data[i].id);
        jQuery('#codigo_emp').val(data[i].codigo_emp);
        jQuery('#empresa').val(data[i].empresa);
        jQuery('#razon_social').val(data[i].razon_social);
        jQuery('#direccion').val(data[i].direccion);
        jQuery('#telefono').val(data[i].telefono);
        jQuery('#ruc').val(data[i].ruc);
        jQuery('#estado').val(data[i].estado);
      });
          jQuery('#empresaModalLabel').text("Editar Empresa");
          jQuery('#btn-save').val("update");
          jQuery('#empresaModal').modal('show');
    })
});

  $("#btn-save").click(function (e) {
     $.ajaxSetup({
         headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
         }
     });
     var type = "GET";     
     var ajaxurl =  'empresas/validarruc/'+ jQuery('#ruc').val();
     $.ajax({
      type: type,
      url: ajaxurl,
      success: function (data) {
          let validarRuc =  parseInt(data); 
          console.log(validarRuc);    
          if(validarRuc > 0  && state != "update" ){
            alert('El RUC ya se encuentra registrado');            
          }  
          else{           
            e.preventDefault();
            let formData = new FormData(document.getElementById("modalFormData"));
            $('#image-input-error').text('');  
              var state = jQuery('#btn-save').val();
              var type = "POST";
              var empresa_id = jQuery('#empresa_id').val();              
              var ajaxurl = 'empresas';              
              if (state == "update") {          
                type = "POST";
                ajaxurl = 'empresas/' + empresa_id;    
                formData.append('_method','PUT');
              }
              $.ajax({
                type: type,
                url: ajaxurl,
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                  var empresa = '';
                $.each(data, function(i){               
                      empresa = '<tr id="'+ data[0].id+'"> <td>'+data[0].id+'</td>  <td> ' +data[0].codigo_emp+ ' </td> <td> ' + data[0].empresa + ' </td>  <td> '+data[0].ruc +'</td> <td> <img src="'+ data[0].icono +'" alt="" width="70" height="50"> </td> <td> ' + data[0].estado +' </td>   <td> '+data[0].usuario +' </td>  <td> '+data[0].updated_at +'</td>';
                      empresa += '<td> <button class="btn btn-info open-modal" value="' + data[0].id + '"><i class="fas fa-edit"></i></button>';
                });
                  if (state == "add") {
                    jQuery('#empresas-list').append(empresa);
                  } else {
                    $("#empresa" + empresa_id).replaceWith(empresa);
                  }
                  jQuery('#modalFormData').trigger("reset");
                  jQuery('#empresaModal').modal('hide');
                },
                error: function(response){
                  console.log(response);
                }
              });   
          } /// fin guarda - actualiza
        },
        error: function(response){
          console.log(response);
          console.log('error');
        }
      });

      console.log('holi1');      

  }); 
});


