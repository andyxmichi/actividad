
$(function () {
    $("#bancos_table").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#bancos_table_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  
    $('#btn-add').click(function () {
      jQuery('#btn-add').click(function () {
      jQuery('#banco_id').prop( "disabled", false );
      jQuery('#ruc').prop( "disabled", false );
      jQuery('#btn-save').val("add");
      jQuery('#modalFormData').trigger("reset");
      jQuery('#bancoModalLabel').text("Nueva Banco");
      jQuery('#bancoModal').modal('show');  
      });
    });
  
    jQuery(function() {
      jQuery("#modalFormData").validate({
        rules: {
          codigo_emp: { required: true,minlength: 3,maxlength: 3},
          banco: { required: true, minlength: 1, maxlength: 250},
          razon_social: { required: true, minlength: 1, maxlength: 500},
          ruc: { required: true,minlength: 13,maxlength: 13,number: true},
          estado: { required: true}
        },
        messages: {
          ruc: {
                  required: "Campo obligatorio",
                  minlength: $.format("Necesitamos por lo menos 13 caracteres"),
                  maxlength: $.format("{0} caracteres son demasiados!"),
                  number: "Solo puede ingresar números"
                },
          empresa: { required: "Campo obligatorio"},
          razon_social: { required: "Campo obligatorio"},
    
        }
       });
    });
  
    jQuery('body').on('click', '.open-modal', function () {
      var banco_id = $(this).val();
      if ($(this).val().length > 0){
        jQuery('#banco_id').prop( "disabled", true );
      }
      $.get('bancos/' + banco_id+'/edit', function (data) {       
        $.each(data, function(i){  
          jQuery('#banco_id').val(data[i].id);
          jQuery('#tipo_cuenta').val(data[i].tipo_cuenta);
          jQuery('#banco').val(data[i].banco);
          jQuery('#cuenta').val(data[i].cuenta);
          jQuery('#persona_encargada').val(data[i].persona_encargada);
          jQuery('#firma').val(data[i].firma);          
          jQuery('#estado').val(data[i].estado);
        });
            jQuery('#bancoModalLabel').text("Editar banco");
            jQuery('#btn-save').val("update");
            jQuery('#bancoModal').modal('show');
      })
  });
  
    $("#btn-save").click(function (e) {
       $.ajaxSetup({
           headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
           }
       });
        var type = "GET";     
        var ajaxurl =  'bancos/validarcuenta/'+ jQuery('#cuenta').val();
        $.ajax({
            type: type,
            url: ajaxurl,
            success: function (data) {
                var state = jQuery('#btn-save').val();
                let validarcuenta =  parseInt(data); 
                console.log(banco_id);    
                if(validarcuenta > 0 && state != "update"){
                    alert('El número de cuenta ya se encuentra registrado');            
                }  
                else{                 
                    e.preventDefault();
                    let formData = new FormData(document.getElementById("modalFormData"));
                    $('#image-input-error').text('');  
                    
                    var banco_id = jQuery('#banco_id').val(); 
                    var type = "POST";                           
                    var ajaxurl = 'bancos';              
                    if (state == "update") {          
                        type = "POST";
                        ajaxurl = 'bancos/' + banco_id;    
                        formData.append('_method','PUT');
                        console.log('updateeee');
                    }
                    $.ajax({
                        type: type,
                        url: ajaxurl,
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            var banco = '';
                            $.each(data, function(i){               
                                banco = '<tr id="'+ data[0].id+'"> <td>'+data[0].id+'</td>  <td> ' +data[0].tipo_cuenta+ ' </td> <td> ' + data[0].banco + ' </td>  <td> '+data[0].cuenta +' </td>  <td> '+data[0].persona_encargada +'</td> <td> <img src="'+ data[0].firma +'" alt="" width="70" height="50"> </td> <td> ' + data[0].estado +' </td>   <td> '+data[0].usuario +' </td>  <td> '+data[0].updated_at +'</td>';
                                banco += '<td> <button class="btn btn-info open-modal" value="' + data[0].id + '"><i class="fas fa-edit"></i></button>';
                            });
                            if (state == "add") {
                                jQuery('#bancos-list').append(banco);
                            } else {
                                $("#banco" + banco_id).replaceWith(banco);
                            }
                            jQuery('#modalFormdata').trigger("reset");
                            jQuery('#bancoModal').modal('hide');
                        },
                        error: function(response){
                            console.log(response);
                        }
                    });  
                }
            },
            error: function(response){
            console.log(response);
            }
        });    
    });    
});
  
  
  