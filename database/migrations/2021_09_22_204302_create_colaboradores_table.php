<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColaboradoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colaboradores', function (Blueprint $table) {
            $table->id();
            $table->string('cedula',10)->unique;
            $table->string('nombre1',50);
            $table->string('nombre2',50);
            $table->string('apellido1',50);
            $table->string('apellido2',50);
            $table->foreignId('fk_cargo');
            $table->date('fecha_nacimiento');
            $table->string('telefono',50);
            $table->string('celular',50);
            $table->string('correo_personal',50);
            $table->string('correo_insititucional',50);
            $table->string('foto');
            $table->string('contacto_emergencia');
            $table->string('parentesco_emergencia');
            $table->string('numero_emergencia',50);
            $table->string('estado',10);
            $table->string('usuario',20);
            $table->timestamps();
            $table->foreign('fk_cargo')->references('id')->on('cargos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colaboradores');
    }
}
