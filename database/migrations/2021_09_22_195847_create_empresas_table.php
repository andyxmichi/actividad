<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->id();
            $table->string('codigo_emp');
            $table->string('ruc',13)->unique();
            $table->string('empresa');
            $table->string('razon_social');
            $table->string('direccion',150);
            $table->string('telefono',50);
            $table->string('icono');
            $table->string('logo');
            $table->string('estado',10);
            $table->string('usuario',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
